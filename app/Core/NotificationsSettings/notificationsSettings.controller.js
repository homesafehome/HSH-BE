const dbConfig = require("../../../config/database");
const Op = dbConfig.Sequelize.Op;
const notificationSettingModel = require("./notificationsSettings.model");
const signalModel = require("../Signals/signals.model");
const { getPagination, getPagingData } = require("../../Shared/pagination");
const { statusDevice } = require("../../../config/enums");

// Retrieve all notifications settings from the database.
exports.findAll = async (req, res, next) => {
  const { page, size, title, message } = req.query;
  let condition = title
    ? { title: { [Op.like]: `%${title}%` } }
    : null;
  if(message){
    condition = {...condition, message: { [Op.like]: `%${message}%` } }
  }
  condition = {
    ...condition,
    deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } }
  };
  try {
    const { limit, offset } = getPagination(page, size);
    const data = await notificationSettingModel.findAndCountAll({
      where: condition,
      limit,
      offset,
      attributes: ['id', 'valueFrom', 'valueTo', 'title', 'message', 'createdAt', 'updatedAt', 'deletionDate'],
      include: { model: signalModel, attributes: ['id', 'name'] }
    });
    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    next(err);
  }
};

// Find a single notification setting with an id
exports.findOne = async (req, res, next) => {
  const id = req.params.id;
  try {
    const data = await notificationSettingModel.findByPk(id, {
      attributes: ['id', 'valueFrom', 'valueTo', 'title', 'message', 'createdAt', 'updatedAt', 'deletionDate'],
      include: { model: signalModel, attributes: ['id', 'name'] }
    });
    res.send(data);
  } catch (err) {
    next(err);
  }
};

exports.create = async (req, res, next) => {
  try {
    // Create and save a notification setting
    const newNotificationSetting = await notificationSettingModel.create({
      signalId: req.body.signalId,
      valueFrom: req.body.valueFrom,
      valueTo: req.body.valueTo,
      title: req.body.title,
      message: req.body.message
    });
    res.send(newNotificationSetting);
  } catch (err) {
    next(err);
  }
};

// Update a notification setting by the id in the request
exports.put = async (req, res, next) => {
  try {
    const notificationSettingUpdated = await notificationSettingModel.update(
      {
        signalId: req.body.signalId,
        valueFrom: req.body.valueFrom,
        valueTo: req.body.valueTo,
        title: req.body.title,
        message: req.body.message
      },
      {
        where: { id: req.params.id },
      }
    );
    res.send(notificationSettingUpdated ? 'Información modificada' : 'No se pudo modificar la información');
  } catch (err) {
    next(err);
  }
};

// Delete a notificationSetting with the specified id in the request
exports.delete = async (req, res, next) => {
  try {
    const data = await notificationSettingModel.destroy({
      where: { id: req.params.id },
    });
    res.send(data);
  } catch (err) {
    next(err);
  }
};

//Patch a notification setting to expire it with deletionDate
exports.expire = async (req, res, next) => {
  try {
    const notificationSettingUpdated = await notificationSettingModel.update(
      {
        deletionDate: new Date(),
      },
      {
        where: { id: req.params.id },
      }
    );
    res.send(notificationSettingUpdated ? 'Información modificada' : 'No se pudo modificar la información');
  } catch (err) {
    next(err);
  }
};

exports.existsNotificationSettingForSignal = async (signalId, value) => {
  const valuesFromAndTo = await findValuesFromAndTo(signalId);
  return valuesFromAndTo.find((valueFromAndTo) => {
    return valueFromAndTo.valueFrom <= value && value <= valueFromAndTo.valueTo;
  });
};

const findValuesFromAndTo = async (signalId) => {
  const valuesFromAndTo = await notificationSettingModel.findAll({
    raw: true,
    where: { signalId: signalId },
  });
  return valuesFromAndTo;
};

//Counts all active notificationSettings
exports.countAll = async (req, res, next) => {
  try {
    const totalItems = await notificationSettingModel.count({
      where: {
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } }
      }
    });
    return totalItems;
  } catch (err) {
    next(err);
  }
};
