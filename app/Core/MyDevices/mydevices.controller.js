const { profiles, usageType } = require('../../../config/enums');
const deviceController = require('../Devices/devices.controller');
const userNotificationsSettingsController = require('../../Features/UserNotificationsSettings/userNotificationsSettings.controller');
const userModel = require('../../Features/Users/users.model');
const userProfileDeviceModel = require('./userProfileDevice.model');
const deviceModel = require('../Devices/devices.model');
const profileModel = require('../Profiles/profiles.model');
const usageModel = require('../UsageTypes/usageTypes.model');
const { getPagination, getPagingData } = require('../../Shared/pagination');
const HttpError = require('../../Shared/ErrorHandler/httpError');
const dbConfig = require('../../../config/database');
const Op = dbConfig.Sequelize.Op;
const Sequelize = dbConfig.Sequelize;

exports.addOwner = async (req, res, next) => {
  try {
    const existingDevice =
      await deviceController.validateDeviceExistBySerialNumberAndPin(
        req,
        res,
        next
      );

    let profileId = getProfileIdByUsageTypeId(existingDevice.usageTypeId);

    let [newUserProfileDevice, userProfileDeviceCreated] =
      await userProfileDeviceModel.findOrCreate({
        defaults: {
          profileId: profileId,
          deviceId: existingDevice.id,
          userId: req.body.userId,
          deletionDate: null,
        },
        where: {
          profileId: profileId,
          deviceId: existingDevice.id,
          deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
        },
      });

    if (userProfileDeviceCreated) {
      userNotificationsSettingsController.enabledAllUserNotifications(
        newUserProfileDevice.id
      );
    } else {
      throw new HttpError('El dispositivo ya esta asociado a un usuario.', 400);
    }

    res.send(newUserProfileDevice);
  } catch (err) {
    next(err);
  }
};

// Retrieve all mydevices from the database.
exports.findAll = async (req, res, next) => {
  try {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    const data = await userProfileDeviceModel.findAndCountAll({
      where: {
        profileId: {
          [Op.or]: [profiles.APARTMENT_OWNER, profiles.BUILDING_OWNER],
        },
        userId: req.body.userId,
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
      },
      include: [
        {
          model: deviceModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: ['id', 'name', 'serialNumber', 'status'],
        },
        {
          model: profileModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: ['id', 'name'],
        },
      ],
      order: [[Sequelize.col('Device.name')]],
      limit,
      offset,
    });
    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    next(err);
  }
};

//Retrieve all mydevices without pagination and named in a specific format
exports.findList = async (req, res, next) => {
  let condition = req.query.profileId
    ? { profileId: req.query.profileId }
    : null;
  if (req.query.type) {
    switch (req.query.type) {
      case 'owner':
        condition = {
          profileId: {
            [Op.or]: [profiles.APARTMENT_OWNER, profiles.BUILDING_OWNER],
          },
        };
        break;
      case 'building':
        condition = {
          profileId: {
            [Op.or]: [
              profiles.BUILDING_OWNER,
              profiles.BUILDING_ADMIN,
              profiles.BUILDING_CONTACT,
            ],
          },
        };
        break;
      default:
        null;
    }
  }
  try {
    const data = await userProfileDeviceModel.findAll({
      where: {
        ...condition,
        userId: req.body.userId,
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
      },
      attributes: [
        ['deviceId', 'key'],
        ['deviceId', 'value'],
        [Sequelize.col('Device.name'), 'label'],
      ],
      include: [
        {
          model: deviceModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: [],
        },
        {
          model: profileModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: [],
        },
      ],
      distinct: true,
      order: [[Sequelize.col('Device.name')]],
    });
    res.send(data);
  } catch (err) {
    next(err);
  }
};

// Find a single mydevice with an id
exports.findOne = async (req, res, next) => {
  try {
    const data = await userProfileDeviceModel.findOne({
      where: { id: req.params.id },
      include: [
        {
          model: deviceModel,
          include: [{ model: usageModel, attributes: ['name'] }],
          attributes: ['name', 'serialNumber', 'status'],
        },
        { model: profileModel, attributes: ['name'] },
      ],
    });
    res.send(data);
  } catch (err) {
    next(err);
  }
};

//Patch a userProfileDevice to expire it with deletionDate
exports.expire = async (req, res, next) => {
  try {
    const [mydeviceUpdated] = await userProfileDeviceModel.update(
      {
        deletionDate: new Date(),
      },
      {
        where: { id: req.params.id },
      }
    );
    res.send(
      mydeviceUpdated
        ? 'Información modificada'
        : 'No se pudo modificar la información'
    );
  } catch (err) {
    next(err);
  }
};

//Counts all active mydevices
exports.countAll = async (req, res, next) => {
  try {
    const totalItems = await userProfileDeviceModel.count({
      where: {
        profileId: {
          [Op.or]: [profiles.APARTMENT_OWNER, profiles.BUILDING_OWNER],
        },
        userId: req.body.userId,
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
      },
      include: [
        {
          model: profileModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: [],
        },
        {
          model: deviceModel,
          where: {
            deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
          },
          attributes: [],
        },
      ],
    });
    return totalItems;
  } catch (err) {
    next(err);
  }
};

// Update a device by the id in the request
exports.rename = async (req, res, next) => {
  try {
    // Rename device in the database
    const [deviceUpdated] = await deviceModel.update(
      {
        name: req.body.name,
      },
      { where: { id: req.params.id } }
    );
    res.send(
      deviceUpdated
        ? 'Información modificada'
        : 'No se pudo modificar la información'
    );
  } catch (err) {
    next(err);
  }
};

exports.findAllUsersAndProfilesFromDeviceId = async (deviceId) => {
  const usersAndProfilesOfThatDevice = await userProfileDeviceModel.findAll({
    where: { deviceId },
    raw: true,
    attributes: [
      'id',
      'deviceId',
      'userId',
      'profileId',
      'createdAt',
      'updatedAt',
      'deletionDate',
    ],
    include: [
      { model: deviceModel, attributes: ['id', 'name'] },
      { model: profileModel, attributes: ['id', 'name'] },
      { model: userModel, attributes: ['id', 'name', 'lastName'] },
    ],
  });
  return usersAndProfilesOfThatDevice;
};

const getProfileIdByUsageTypeId = (usageTypeId) => {
  return usageTypeId == usageType.APARTMENT
    ? profiles.APARTMENT_OWNER
    : profiles.BUILDING_OWNER;
};

exports.validateOwnerOfDevice = async (req, res, next) => {
  try {
    const { existingDevice, ownerUserId: userId } = req;
    let profileId = getProfileIdByUsageTypeId(existingDevice.usageTypeId);

    const ownerExist = await userProfileDeviceModel.findOne({
      where: { userId, profileId, deviceId: existingDevice.id },
      attributes: ['id', 'deviceId', 'userId', 'profileId'],
    });
    return ownerExist.toJSON();
  } catch (err) {
    throw new HttpError('Usted no es propietario del dispositivo', 400);
  }
};


// Retrieve all mydevices from the database.
exports.countAllStates = async (req, res, next) => {
  try {
    const { page, size } = req.query;
    const data = await deviceModel.findAll({
      where:{
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
        id: {[Op.in]: [
          Sequelize.literal('Select Distinct deviceId From UsersProfilesDevices Where userId = ' + req.body.userId +
            ' And (deletionDate is null Or deletionDate >= now())')
        ]}
      },
      attributes: ['status', [Sequelize.fn('COUNT', Sequelize.col('id')), 'count'] ],
      group: ['status'],
      raw:true,
      order: ['status']
    });
    res.send(data);
  } catch (err) {
    next(err);
  }
};
