const {
  generateAccessToken,
  verifyAccessToken,
  verifyRefreshToken,
} = require('../Authentications/Jwt/utils');
const { validateSessionExist } = require('./Sessions/sessions.controller');
const { errorsJWT } = require('../../../config/enums');
const HttpError = require('../../Shared/ErrorHandler/httpError');

const authenticate = (req, res, next) => {
  try {
    let accessToken = req.headers['x-access-token'];
    const payloadAccessToken = verifyAccessToken(accessToken);
    req.body.userId = payloadAccessToken.userLogged.id;
    next();
  } catch (err) {
    if (err.message === errorsJWT.EXPIRED) {
      next(new HttpError('Usuario no autenticado', 401));
    }
    next(err);
  }
};

const refresh = async (req, res, next) => {
  try {
    let refreshToken = req.headers['x-refresh-token'];
    const payloadRefreshToken = verifyRefreshToken(refreshToken);

    await validateSessionExist(payloadRefreshToken.userLogged.id, refreshToken);

    let newToken = await generateAccessToken(payloadRefreshToken.userLogged);

    res.set({
      'x-access-token': newToken,
      'x-refresh-token': refreshToken,
    });
    next();
  } catch (err) {
    next(new HttpError('Usuario no autenticado', 401));
  }
};

module.exports = { authenticate };
