const errorMiddleware = (error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  if (!error.code) error.code = 500;
  if (error.code === 500) {
    console.log(error.message);
    error.message = 'Ha ocurrido un error en el sistema!';
  }
  res.status(error.code).send({ message: error.message });
};

module.exports = { errorMiddleware };
