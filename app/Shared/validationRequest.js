const HttpError = require('./ErrorHandler/httpError');
const validateIdParam = (schema) => {
  return (req, res, next) => {
    const { error, value } = schema.validate(req.params);
    if (error) {
      throw new HttpError(error.message, 400);
    }
    next();
  };
};
const validateBody = (schema) => {
  return (req, res, next) => {
    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new HttpError(error.message, 400);
    }
    next();
  };
};
module.exports = { validateIdParam, validateBody };
