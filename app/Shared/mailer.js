var nodemailer = require('nodemailer');

const createHtml = (title, message, pin) => {
  return `<div style="background:#4DBFBF;background-color:#4DBFBF;margin:0px auto;max-width:600px;">
<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
  style="background:#4DBFBF;background-color:#4DBFBF;width:100%;">
  <tbody>
    <tr>
      <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
        <div class="dys-column-per-100 outlook-group-fix"
          style="direction:ltr;display:inline-block;font-size:13px;text-align:left;vertical-align:top;width:100%;">
          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;"
            width="100%">
            <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                    style="border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                      <tr>
                        <td style="width:216px;"><img alt="Descriptive Alt Text" height="189"
                            src="http://wp.airelink.com.ar/hsh.png"
                            style="border:none;display:block;font-size:13px;height:189px;outline:none;text-decoration:none;width:100%;"
                            width="216"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div
                    style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:36px;line-height:1;text-align:center;">
                    ${title}</div>
                </td>
              </tr>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div
                    style="color:#187272;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:16px;line-height:20px;text-align:center;">
                    ${message}</div>
                </td>
              </tr>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                  vertical-align="middle">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                    style="border-collapse:separate;line-height:100%;width:200px;">
                    <tbody>
                      <tr>
                        <td align="center" bgcolor="#178F8F" role="presentation"
                          style="background-color:#178F8F;border:none;border-radius:4px;cursor:auto;padding:10px 25px;"
                          valign="middle"><a
                            style="background:#178F8F;color:#ffffff;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:16px;font-weight:bold;line-height:30px;margin:0;text-decoration:none;text-transform:none;"
                            >${pin}</a></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

      </td>
    </tr>
  </tbody>
</table>
</div>`;
};

const sendMail = async (to, subject, title, message, pin) => {
  let transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE,
    host: process.env.EMAIL_HOST,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  // send mail with defined transport object
  return await transporter.sendMail({
    from: process.env.EMAIL_USER,
    to,
    subject,
    text: 'Test',
    html: createHtml(title, message, pin),
  });
};

module.exports = { sendMail };
