const dbConfig = require("../../../config/database");
const userNotificationSettingModel = require("./userNotificationsSettings.model");
const { signals, userNotificationStatus } = require('../../../config/enums');
const userProfileDeviceModel = require("../../Core/MyDevices/userProfileDevice.model");
const signalModel = require("../../Core/Signals/signals.model");
const Op = dbConfig.Sequelize.Op;
const Sequelize = dbConfig.Sequelize;

exports.getUserNotificationSetting = async (userProfileDeviceId, signalId) => {
    const userDevice = await userNotificationSettingModel.findOne({
        raw: true,
        where: { userProfileDeviceId, signalId },
    });
    return userDevice;
};

exports.enabledAllUserNotifications = async (userProfileDeviceId) => {
    try {
        const enableNotificationOfOnlineSignal = await userNotificationSettingModel.bulkCreate([
            {
                userProfileDeviceId,
                signalId: signals.ONLINE,
                status: userNotificationStatus.ON
            },
            {
                userProfileDeviceId,
                signalId: signals.OPEN_CLOSE_APERTURE,
                status: userNotificationStatus.ON
            },
            {
                userProfileDeviceId,
                signalId: signals.CARBON_MONOXIDE,
                status: userNotificationStatus.ON
            }
        ]);
    } catch (err) {
        throw Error(err.message);
    }

}

// Retrieve all usernotificationssettings related to the Device id from the database.
exports.findAll = async (req, res, next) => {
  try {
    const data = await userNotificationSettingModel.findAll({
      where: {
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } }
      },
      attributes: ['id', 'signalId', 'status', [Sequelize.col('Signal.name'), 'signal'],],
      include: [{
        model: userProfileDeviceModel,
        where: { userId: req.body.userId,
          deviceId: req.query.deviceId,
          deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } }, },
        attributes: []
      },
      {
        model: signalModel,
        where: { deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } } },
        attributes: []
      }],
      distinct: true,
      order: [[Sequelize.col('Signal.name')]],
    });
    res.send(data);
  }
  catch (err) {
    next(err);
  }
};

// Patch a userNotificationsSettings by the signalId in the request
exports.patch = async (req, res, next) => {  console.log('ID '+req.params.id+' Signal '+req.query.signalId);
  try {
    const settingsPatched = await userNotificationSettingModel.update(
      {
        status: req.body.status,
      },
      {
        where: { id: req.params.id,
          signalId: req.query.signalId },
      }
    );
    res.send(settingsPatched);
  } catch (err) {
    next(err);
  }
};
