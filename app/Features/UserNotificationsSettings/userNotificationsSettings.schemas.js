const joi = require("joi");


const schemaPatchUserNotificationsSettings = joi.object({
  signalId: joi.number(),
  status: joi.number().required(),
  userId: joi.number(),
});

const schemaIdParam = joi.object().keys({
  id: joi.string().required(),
});

module.exports = {
  schemaPatchUserNotificationsSettings,
  schemaIdParam,
};
