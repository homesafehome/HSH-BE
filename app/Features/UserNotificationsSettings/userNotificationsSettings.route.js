const router = require("express").Router();
const usernotificationssettings = require("./userNotificationsSettings.controller");
const { schemaIdParam, schemaPatchUserNotificationsSettings } = require("./userNotificationsSettings.schemas");
const { validateIdParam, validateBody } = require("../../Shared/validationRequest");

// Retrieve all usernotificationssettings
router.get("/", usernotificationssettings.findAll);

// Patch usernotificationssettings with id
router.patch("/:id",
  [validateIdParam(schemaIdParam), validateBody(schemaPatchUserNotificationsSettings)],
  usernotificationssettings.patch);

module.exports = router;
