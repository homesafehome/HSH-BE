/**
* @swagger
*  /usernotificationssettings:
*    get:
*      description: Gets UserNotificationsSettings for the device
*      tags: [User Notifications Settings]
*      parameters:
*        - in: query
*          name: deviceId
*          required: true
*          description: Device id
*          schema:
*            type: integer
*      responses:
*        200:
*          description: All devices count
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  id:
*                    type: integer
*                    description: UserNotificationsSettings id
*                    example: 1
*                  signalId:
*                    type: integer
*                    description: Signal id
*                    example: 1
*                  status:
*                    type: integer
*                    description: status value (0 OFF ; 1 ON)
*                    example: 1
*                  signal:
*                    type: string
*                    description: Signal name
*                    example: SIGNAL
*        500:
*          description: Internal server error
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  message:
*                    type: string
*                  internal code:
*                    type: string
*
* /usernotificationssettings/{id}:
*  patch:
*    description: Patches the status value in userNotificationsSettings with id and signalId
*    tags: [User Notifications Settings]
*    parameters:
*       - in: path
*         name: id
*         required: true
*         description: userNotificationsSettings id
*         schema:
*           type: integer
*       - in: query
*         name: signalId
*         required: true
*         description: signal id
*         schema:
*           type: integer
*    requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             type: object
*             required:
*               - status
*             properties:
*               status:
*                 type: integer
*                 description: status value (0 OFF ; 1 ON)
*                 example: 1
*    responses:
*       200:
*         description: returns code 1
*         content:
*       500:
*         description: Internal server error
*         content:
*           application/json:
*             schema:
*               type: object
*               properties:
*                 message:
*                   type: string
*                 internal code:
*                   type: string
*/
