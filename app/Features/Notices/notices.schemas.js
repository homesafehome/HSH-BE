const joi = require("joi");

const schemaCreateNotice = joi.object({
  title: joi.string().required(),
  message: joi.string().required(),
  deviceId: joi.number().required(),
  userId: joi.number(),
});

const schemaUpdateNotice = joi.object({
  title: joi.string().required(),
  message: joi.string().required(),
  deviceId: joi.number().required(),
  userId: joi.number(),
});

const schemaIdParam = joi.object().keys({
  id: joi.string().required(),
});

module.exports = {
  schemaCreateNotice,
  schemaUpdateNotice,
  schemaIdParam,
};
