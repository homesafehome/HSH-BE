const notifications = require("../Notifications/notifications.controller");
const claims = require("../Claims/claims.controller");
const notices = require("../Notices/notices.controller");
const users = require("../Users/users.controller");
const devices = require("../../Core/Devices/devices.controller");
const mydevices = require("../../Core/MyDevices/mydevices.controller");
const mycontacts = require("../../Core/MyContacts/mycontacts.controller");
const notificationsettings = require("../../Core/NotificationsSettings/notificationsSettings.controller");
const profiles = require("../../Core/Profiles/profiles.controller");
const signals = require("../../Core/Signals/signals.controller");
const usagetypes = require("../../Core/UsageTypes/usageTypes.controller");
const contacttypes = require("../../Core/ContactTypes/contactTypes.controller");

exports.getDevices = async (req, res, next) => {
  try {
    let counts = await devices.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getMyDevices = async (req, res, next) => {
  try {
    let counts = await mydevices.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getClaims = async (req, res, next) => {
  try {
    let counts = await claims.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getNotices = async (req, res, next) => {
  try {
    let counts = await notices.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getNotifications = async (req, res, next) => {
  try {
    let counts = await notifications.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getContacts = async (req, res, next) => {
  try {
    let counts = await mycontacts.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getUsers = async (req, res, next) => {
  try {
    let counts = await users.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getProfiles = async (req, res, next) => {
  try {
    let counts = await profiles.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getSignals = async (req, res, next) => {
  try {
    let counts = await signals.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getNotificationSettings = async (req, res, next) => {
  try {
    let counts = await notificationsettings.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getUsageTypes = async (req, res, next) => {
  try {
    let counts = await usagetypes.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};

exports.getContactTypes = async (req, res, next) => {
  try {
    let counts = await contacttypes.countAll(req, res, next);
    res.send({count: counts});
  } catch (err) {
    next(err);
  }
};
