const dbConfig = require('../../../config/database');
const userModel = require('./users.model');
const { getPagination, getPagingData } = require('../../Shared/pagination');
const HttpError = require('../../Shared/ErrorHandler/httpError');
const { sendMail } = require('../../Shared/mailer');

const Op = dbConfig.Sequelize.Op;

// Create and Save a new user
exports.create = async (req, res, next) => {
  try {
    // Create and save a user
    const newUser = await userModel.create({
      email: req.body.email,
      phone: req.body.phone,
      password: req.body.password,
      name: req.body.name,
      lastName: req.body.lastName,
    });
    res.send(newUser);
  } catch (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      err = new HttpError(
        'Oops, parece que ya existe un usuario con este email, por favor trata de iniciar sesión o reestablecer contraseña',
        400
      );
    }
    next(err);
  }
};

// Retrieve all Users from the database.
exports.findAll = async (req, res, next) => {
  const { page, size, email, name, lastName } = req.query;
  let condition = email ? { email: { [Op.like]: `%${email}%` } } : null;
  if (name) {
    condition = { ...condition, name: { [Op.like]: `%${name}%` } };
  }
  if (lastName) {
    condition = { ...condition, lastName: { [Op.like]: `%${lastName}%` } };
  }
  condition = {
    ...condition,
    deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
  };

  try {
    const { limit, offset } = getPagination(page, size);
    const data = await userModel.findAndCountAll({
      where: condition,
      limit,
      offset,
    });
    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    next(err);
  }
};

// Find a single User with an id
exports.findOne = async (req, res, next) => {
  try {
    const data = await userModel.findByPk(req.params.id);
    res.send(data);
  } catch (err) {
    next(err);
  }
};

// Update a User by the id in the request
exports.update = async (req, res, next) => {
  try {
    const [userUpdated] = await userModel.update(
      {
        email: req.body.email,
        phone: req.body.phone,
        name: req.body.name,
        lastName: req.body.lastName,
      },
      {
        where: { id: req.params.id },
      }
    );
    res.send(
      userUpdated
        ? 'Información modificada'
        : 'No se pudo modificar la información'
    );
  } catch (err) {
    next(err);
  }
};

// Patch a user by the id in the request
exports.patch = async (req, res, next) => {
  try {
    const { id: userId } = await validatePin(req.body.email, req.body.pin);

    if (userId) {
      const [userPatched] = await userModel.update(
        {
          password: req.body.password,
          pin: null,
          deletionDate: null,
        },
        {
          where: { id: userId },
        }
      );
      res.send(
        userPatched
          ? 'Información modificada'
          : 'No se pudo modificar la información'
      );
    }
  } catch (err) {
    next(err);
  }
};

const validatePin = async (email, pin) => {
  try {
    const userExist = await userModel.findOne({
      where: { email, pin },
      attributes: ['id', 'email', 'name', 'lastName'],
    });
    return userExist.toJSON();
  } catch (err) {
    throw new HttpError('Credenciales inválidas.', 404);
  }
};

// Patch a user by the id in the request
exports.createPin = async (req, res, next) => {
  try {
    const { id: userId } = await validateEmail(req.body.email);

    if (userId) {
      const generatedPin = (Math.random() + 1).toString(36).substring(3);
      await userModel.update(
        {
          pin: generatedPin,
        },
        {
          where: { id: userId },
        }
      );

      const emailSent = await sendMail(
        req.body.email,
        'Ingreso a HomeSafeHome',
        'Se ha generado un pin para recuperar contraseña',
        'A continuación te enviamos el pin que sera necesario para configurar tu contraseña.',
        generatedPin
      );
      res.send(
        emailSent
          ? 'Se envió un mail con el PIN generado'
          : 'No se pudo enviar mail con nuevo PIN'
      );
    }
  } catch (err) {
    next(err);
  }
};

const validateEmail = async (email) => {
  try {
    const userExist = await userModel.findOne({
      where: { email },
      attributes: ['id', 'email', 'name', 'lastName'],
    });
    return userExist.toJSON();
  } catch (err) {
    throw new HttpError('Credenciales inválidas.', 404);
  }
};

// Delete a User with the specified id in the request
exports.delete = async (req, res, next) => {
  try {
    const data = await userModel.destroy({
      where: { id: req.params.id },
    });
    res.send(data ? 'Registro eliminado' : 'No se pudo eliminar el registro');
  } catch (err) {
    next(err);
  }
};

//Patch a user to expire it with deletionDate
exports.expire = async (req, res, next) => {
  try {
    const [userPatched] = await userModel.update(
      {
        deletionDate: new Date(),
      },
      {
        where: { id: req.params.id },
      }
    );
    res.send(
      userPatched
        ? 'Información modificada'
        : 'No se pudo modificar la información'
    );
  } catch (err) {
    next(err);
  }
};

exports.validateUserCredentials = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const userExist = await userModel.findOne({
      where: {
        email,
        password,
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
      },
      attributes: ['id', 'email', 'name', 'lastName'],
    });
    return { userLogged: userExist.toJSON() };
  } catch (err) {
    throw new HttpError('Credenciales inválidas.', 404);
  }
};

exports.findOrCreateUser = async (req, res, next) => {
  try {
    const generatedPin = (Math.random() + 1).toString(36).substring(3);
    let [user, userCreated] = await userModel.findOrCreate({
      defaults: {
        email: req.body.email,
        pin: generatedPin,
      },
      where: {
        email: req.body.email,
      },
    });
    if (userCreated)
      await sendMail(
        req.body.email,
        'Ingreso a HomeSafeHome',
        'Has sido agregado como contacto',
        'A continuación te enviamos el pin que sera necesario para configurar tu contraseña.',
        generatedPin
      );

    return user;
  } catch (err) {
    next(err);
  }
};

//Counts all active users
exports.countAll = async (req, res, next) => {
  try {
    const totalItems = await userModel.count({
      where: {
        deletionDate: { [Op.or]: { [Op.gte]: new Date(), [Op.is]: null } },
      },
    });
    return totalItems;
  } catch (err) {
    next(err);
  }
};
