const joi = require("joi");

const schemaCreateUser = joi.object({
  email: joi.string().email().required(),
  phone: joi.string().required(),
  password: joi.string().required(),
  name: joi.string().required(),
  lastName: joi.string().required(),
});

const schemaUpdateUser = joi.object({
  email: joi.string().email().required(),
  phone: joi.string().required(),
  name: joi.string().required(),
  lastName: joi.string().required(),
});

const schemaPatchUser = joi.object({
  email: joi.string().required(),
  password: joi.string().required(),
  pin: joi.string(),
});

const schemaCreatePin = joi.object({
  email: joi.string().required(),
});

const schemaIdParam = joi.object().keys({
  id: joi.string().required(),
});

module.exports = {
  schemaCreateUser,
  schemaUpdateUser,
  schemaPatchUser,
  schemaCreatePin,
  schemaIdParam,
};
